public class PercolationStats {
    private int iterations;
    private double[] counts;
    private double mean;
    private double stddev;

    /**
     * Perform T independent computational experiments on an N-by-N grid
     *
     * @param N
     * @param T
     */
    public PercolationStats(int N, int T) {
        iterations = T;
        counts = new double[T];

        int count = 0;

        for (int i = 0; i < T; i++) {

            Percolation p = new Percolation(N);

            while (!p.percolates()) {
                int random1 = StdRandom.uniform(1,N+1);
                int random2 = StdRandom.uniform(1,N+1);

                p.open(random1, random2);
                count++;

                counts[i] = (double) count / (double) (N * N);
            }
        }
    }

    /**
     * Sample mean of percolation threshold
     *
     * @return
     */
    public double mean() {
        double sum = 0.0;

        for (int i = 0; i < counts.length; i++) {
            sum += counts[i];
        }

        return (sum / iterations);
    }

    /**
     * Sample standard deviation of percolation threshold
     *
     * @return
     */
    public double stddev() {
        double sum = 0.0;

        for (int i = 0; i < counts.length; i++) {
            sum += ((counts[i] - this.mean) * (counts[i] - this.mean));
        }

        return Math.sqrt(sum / (double) (this.iterations - 1));
    }

    /**
     * Returns lower bound of the 95% confidence interval
     *
     * @return
     */
    public double confidenceLo() {
        return this.mean - ((1.96 * this.stddev) / Math.sqrt((double) this.iterations));
    }

    /**
     * Returns upper bound of the 95% confidence interval
     *
     * @return
     */
    public double confidenceHi() {
        return this.mean + ((1.96 * this.stddev) / Math.sqrt((double) this.iterations));
    }

    public static void main(String[] args) // test client, described below
    {
        int N = 0;
        int T = 0;

        try {
            N = Integer.parseInt(args[0]);
            T = Integer.parseInt(args[1]);

        } catch (IndexOutOfBoundsException e) {
            System.out.println("Usage: PercolationStats.java N T");
        }

        PercolationStats p = new PercolationStats(N, T);

        p.mean = p.mean();
        p.stddev = p.stddev();

        double confidenceLo = p.confidenceLo();
        double confidenceHi = p.confidenceHi();

        System.out.println("mean                       = " + p.mean);
        System.out.println("stddev                     = " + p.stddev);
        System.out.println("95% confidence interval    = " + confidenceLo + ", " + confidenceHi);
    }
}