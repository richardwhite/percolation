import java.awt.*;

public class Percolation {
    private WeightedQuickUnionUF connections;
    private boolean[][] grid;
    private int v1;
    private int v2;
    private int size;

    /**
     * Create a N-by-N grid, with all sites blocked
     *
     * @param N
     */
    public Percolation(int N) {
        this.size = N;

        this.grid = new boolean[N][N];

        this.connections = new WeightedQuickUnionUF(((N * N) + 2));

        StdDraw.setCanvasSize(800, 600);
        StdDraw.setScale(0, N);

        for(int i = 0; i <= N; i++)
        {
            for(int j = 0; j <= N; j++)
            {
                StdDraw.filledSquare(j, i, 1);
            }
        }

        StdDraw.setPenColor(Color.WHITE);

        System.out.println("N=" + N);

        //- Initialise virtual sites to connect the top and bottom row of the grid respectively
        this.v1 = (N * N);
        this.v2 = (N * N) + 1;
    }

    /**
     * Open site (row i, column j) if it is not already
     *
     * @param i (row)
     * @param j (column)
     */
    public void open(int i, int j) {
        //- Check that the specified site is not out of bounds before attempting access
        if((i < 1 || i > size) || (j < 1 || j > size)) {
            throw new IndexOutOfBoundsException();
        }

        if (!isOpen(i, j)) {

            this.grid[i-1][j-1] = true;

            System.out.println("j (column)= " + j + " i (row)= " + i);

            //- Check to see if we are opening a site on the first row and join to v1

            if (i==1) {
                connections.union(xyTo1D(i-1, j-1), v1);
            }

            //- Check to see if we are opening a site on the last row and join to v2

            if (i==size) {
                connections.union(xyTo1D(i-1, j-1), v2);
            }

            //- Check to determine open neighbouring sites and connect them if required

            //- Above
            if (i>1 && isOpen(i-1, j)) {
                connections.union(xyTo1D(i-1, j-1), xyTo1D(i-2, j-1));
            }

            //- Below
            if (i<size && isOpen(i+1, j)) {
                connections.union(xyTo1D(i-1, j-1), xyTo1D(i, j-1));
            }

            //- Left
            if (j>1 && isOpen(i, j-1)) {
                connections.union(xyTo1D(i-1, j-1), xyTo1D(i-1, j-2));
            }

            //- Right
            if (j<size && isOpen(i, j+1)) {
                connections.union(xyTo1D(i-1, j-1), xyTo1D(i-1, j+2));
            }

            for(int x = 1; x < this.size; x++)
            {
                for(int z = 1; z < this.size; z++)
                {
                    if(isOpen(x,z)){
                        if(isFull(x,z)){
                            StdDraw.setPenColor(Color.BLUE);
                        } else{
                            StdDraw.setPenColor(Color.WHITE);
                        }
                        StdDraw.filledSquare(x-0.5, z-0.5, 0.5);
                    }
                }
            }

            try {
                Thread.sleep(300);
            } catch(InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /**
     * Is site (row i, column j) open?
     *
     * @param i (row)
     * @param j (column)
     * @return
     */
    public boolean isOpen(int i, int j) {
        //- Check that the specified site is not out of bounds before attempting access
        if((i < 1 || i > size) || (j < 1 || j > size)) {
            throw new IndexOutOfBoundsException();
        }
        return this.grid[i-1][j-1];
    }

    /**
     * Is site (row i, column j) open?
     *
     * @param i (row)
     * @param j (column)
     * @return
     */
    public boolean isFull(int i, int j)
    {
        //- Check that the specified site is not out of bounds before attempting access
        if((i < 1 || i > size) || (j < 1 || j > size)) {
            throw new IndexOutOfBoundsException();
        } else {
            if(connections.connected(xyTo1D(i-1, j-1), v2)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Does the system percolate?
     *
     * @return
     */
    public boolean percolates() {
        if (connections.connected(v1, v2)) {
            return true;
        }
        return false;
    }

    /**
     * Convert 2-dimensional row/column pair to a 1 dimensional object index
     *
     * @param i (row)
     * @param j (column)
     * @return
     */
    private int xyTo1D(int i, int j) {
        return (i * (size) + j);
    }
}